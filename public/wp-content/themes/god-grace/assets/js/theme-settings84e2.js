(function ($) {  

     "use strict"; 

    /* Tooltip ------------------------- */

    function godgrace_toolTip() {
        $('.godgrace_tooltip').tooltipster();
        $('.tipUp').tooltipster({ position: 'top' });
        $('.tipDown').tooltipster({ position: 'bottom' });
        $('.tipRight').tooltipster({ position: 'right' });
        $('.tipLeft').tooltipster({ position: 'left' });
    }

    /* Retina ------------------------- */

    function godgrace_retinaRatioCookies() {
        var godgrace_DevicePixelRatio = !!window.devicePixelRatio ? window.devicePixelRatio : 1;
        if (!$.cookie("pixel_ratio")) {
            if (godgrace_DevicePixelRatio > 1 && navigator.cookieEnabled === true) {
                $.cookie("pixel_ratio", godgrace_DevicePixelRatio, {expires : 360});
                location.reload();
            }
        }
    }

    /* Post Format Gallery ------------------------- */

    function godgrace_pf_gallery() { 
        $('.pfi_gallery').imagesLoaded( function() {
            $('.pfi_gallery').flexslider({
                animation: 'fade',
                animationSpeed: 500,
                slideshow: false,
                smoothHeight: false,
                controlNav: false,
                directionNav: true,               
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>'                
            });
        });    
    }


    /* Go top scroll ------------------------- */

    function godgrace_go_top_scroll() { 

        var godgrace_PageScroll = false,
            $godgrace_PageScrollElement = $('#godgrace_go_top_scroll_btn');

        $godgrace_PageScrollElement.click(function(e) {
            $('body,html').animate({ scrollTop: "0" }, 750, 'easeOutExpo' );
            e.preventDefault();
        });

        $(window).scroll(function() {
            godgrace_PageScroll = true;
        });
   
    }
       
    /* Share Post Link ------------------------- */

    function godgrace_share_post_links() {

        $(".godgrace_post_share").each(function(){ 

            var $this = $(this),
                godgrace_ElementId = $this.attr("data-postid"),
                godgrace_ClickElement = '.post-share-id-'+godgrace_ElementId,
                godgrace_OpenElement = '.godgrace-share-id-box-'+godgrace_ElementId+' ul';

            $(godgrace_ClickElement).addClass('active');

            $(godgrace_ClickElement).click( function(e){
                e.preventDefault();
                if ($(this).hasClass("active") ) {
                    $(godgrace_OpenElement).animate({ left: '30' }, 500);                
                    $(this).removeClass("active");
                } else {
                    $(godgrace_OpenElement).animate({ left: '-300' }, 500);
                    $(this).addClass("active");
                }
                return false;
            });

        });    

    }

    /* Post Like ------------------------- */

    function godgrace_post_like() {

        $('.godgrace-love').click(function() {
            var el = $(this);
            if( el.hasClass('loved') ) return false;
            
            var post = {
                action: 'godgrace_love',
                post_id: el.attr('data-id')
            };
            
            $.post(godgrace_get_ajax_full_url.ajaxurl, post, function(data){
                el.find('.label').html(data);
                el.addClass('loved');
            });

            return false;
        }); 
    }

    /* Element Horizontal and Verticel Center ------------------------- */  

    function godgrace_js_H_Center() {

        $(".godgrace_js_center").each(function(){
            var $this = $(this),
                godgrace_ElementWidth = $this.width(),
                godgrace_ElementMargin = godgrace_ElementWidth/-2;
            $this.css('margin-left',godgrace_ElementMargin);
        });
       
    }

    function godgrace_js_V_Center() {

        $(".godgrace_js_center_top").each(function(){

            var $this = $(this),
                godgrace_ElementWidth = $this.height(),
                godgrace_ElementMargin = godgrace_ElementWidth/-2;

            $this.css('margin-top',godgrace_ElementMargin);

        });

        $(".godgrace_js_center_bottom").each(function(){

            var $this = $(this),
                godgrace_ElementWidth = $this.height(),
                godgrace_ElementMargin = godgrace_ElementWidth/-2;

            $this.css('margin-bottom',godgrace_ElementMargin);

        });
       
    }

    /* Image Lightbox ------------------------- */  

    function godgrace_magnificPopup() {

        $('.godgrace_popup_img').magnificPopup({
            type: 'image'
        });

        $('.godgrace_popup_gallery').magnificPopup({
            type: 'image',           
            gallery:{
                enabled:true,
                tCounter:''
            },
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out'
            }
        });

        $('.popup-youtube, .popup-vimeo, .popup-gmaps,.godgrace_popup_video').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });


        $('.godgrace_popup_gallery_alt').magnificPopup(
            {
                delegate: 'a',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                mainClass: 'mfp-img-mobile',
                gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1]
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {
                    return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                }
            }
        });

    }

    /* WordPress Gallery ------------------------- */

    function godgrace_WPGallery() {
        if ( $('.gallery').length ){ 

            var godgrace_LayoutModeStyle = 'fitRows';

            if ($("body").hasClass('godgrace_img_gallery_masonry')) {
                godgrace_LayoutModeStyle = 'masonry';
            }

            $('.gallery').imagesLoaded( function() {
                $('.gallery').isotope({
                    itemSelector: '.gallery-item',
                    layoutMode: godgrace_LayoutModeStyle
                });
            });

        }
    }

    /* Sticky Header ------------------------- */  


    function godgrace_sticky_header(){
        if( $('body').hasClass('godgrace_stickyOn') ){  

            var godgrace_header_height = 0,
                headerHeight = $('.godgrace_nav').innerHeight(),

            godgrace_header_height = headerHeight;

            var start_y = godgrace_header_height,
                window_y = $(window).scrollTop(),
                wpAdminBarHeight = 0;

            if ( $('#wpadminbar').length ){ 
                wpAdminBarHeight = $('#wpadminbar').innerHeight();
            }
    
            if( window_y > start_y ){

                if( ! ($('#godgrace_Header').hasClass('sticky-on'))){                                                
                    $('#godgrace_Header')
                        .addClass('sticky-on')
                        .css('top',-80)
                        .animate({'top': wpAdminBarHeight },300); 
                }
            }
            else {
                if($('#godgrace_Header').hasClass('sticky-on')) {
                    $('#godgrace_Header')
                        .removeClass('sticky-on')
                        .css('top', 0);                    
                }   
            }
        }
    }

    /* Portfolio Page ------------------------- */  

    function godgrace_portfolio_items() { 

        if ($(window).width() < 768) {
            $('div .godgrace_horizontal_menu').addClass('h_responsive');
        }
    }

    function godgrace_universal_filter_items_menu() {
        $('.godgrace_universal_filter_items_menu a').click(function(){
            var selector = $(this).attr('data-filter');
            $('.godgrace_universal_filter_items_section').isotope({filter: selector});
            $('.godgrace_universal_filter_items_menu a.godgrace-active-sort').removeClass('godgrace-active-sort');
            $(this).addClass('godgrace-active-sort');
            return false;
        });     
    }

    function godgrace_smooth_scroll() {
        $('a.smooth-scroll').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                    scrollTop: target.offset().top - 140
                }, 1000);
                return false;
                }
            }

        });
    }

    function godgrace_list_widgets() {

        $(".sidebar").children(".widget_meta,.widget_categories,.widget_pages,.widget_archive,.widget_recent_comments,.widget_recent_entries,.widget_nav_menu,.widget_product_categories,.widget_layered_nav_filters,.archives-link,.widget_rss,.swmsc_custom_cat_widget").addClass("godgrace_list_widgets");
    }  



    function godgrace_sermons_single() { 

        var godgrace_sermon_video = $('.godgrace_sermon_video'),
            godgrace_sermon_audio = $('.godgrace_sermon_audio');

        if (godgrace_sermon_audio.length) {   

            var mediaPlayer = new MediaElementPlayer('.sermonAudio');
            if ($('.sermonAudio').length) {         
                $(".sermonAudio").css('max-width', '100%');
                setTimeout(function() {            
                    $(window).trigger('resize');
                    godgrace_sermon_audio.hide();
                    if (window.location.hash == '#getAudio') {
                        godgrace_sermon_video.hide();
                        godgrace_sermon_audio.show();
                        mediaPlayer.play();       
                        $(window).trigger('resize');
                    }
                }, 400);             
            }

        } 

        if (godgrace_sermon_video.length) {
            var video_on = godgrace_sermon_video.html().replace('autoplay=0', 'autoplay=1'),
                video_off = godgrace_sermon_video.html().replace('autoplay=1', 'autoplay=0');       
        }    

        $('.playSermonVideo').click(function(event) {
            event.preventDefault();
            if (godgrace_sermon_audio.length) {       
                mediaPlayer.pause();
                godgrace_sermon_audio.hide();
            }
            godgrace_sermon_video.html(video_on).show();
        });
            
        $('.playSermonAudio').click(function(event) {
            event.preventDefault();       
            godgrace_sermon_video.hide();

            if (godgrace_sermon_video.length) { 
                godgrace_sermon_video.html(video_off); 
            }

            godgrace_sermon_audio.show();      
            mediaPlayer.play();       
            $(window).trigger('resize');
        });
        
    }

    /* Document ready load functions =================== */

    $(document).ready(function() {  

        $(".fitVids").fitVids();
        godgrace_toolTip();
        godgrace_retinaRatioCookies();
        godgrace_pf_gallery();
        godgrace_go_top_scroll();
        godgrace_share_post_links();
        godgrace_post_like();
        godgrace_js_H_Center();
        godgrace_js_V_Center();
        godgrace_magnificPopup();
        godgrace_WPGallery();
        godgrace_sticky_header();
        godgrace_portfolio_items();
        godgrace_universal_filter_items_menu();
        godgrace_smooth_scroll();
        godgrace_list_widgets();
        godgrace_sermons_single();
    });

    /* Scroll Events ---------- */

    $(window).scroll(function(){    
        godgrace_sticky_header();
    });


    function godgrace_infinite_scroll() {

        var $godgrace_ContainerInfiniteScroll = $("#godgrace-item-entries");
        $godgrace_ContainerInfiniteScroll.infinitescroll({
            loading: {
                msg: null,
                finishedMsg: null,
                msgText: '<div class="godgrace_infiniteScroll_loader"><span class="godgrace_infiniteScroll_loader_wrap"><span class="godgrace_infiniteScroll_loader_holder"></span></span></div>',
            },
            navSelector: "div.godgrace_infiniteScroll_pagination",
            nextSelector: "div.godgrace_infiniteScroll_pagination div.post-next a",
            itemSelector: ".godgrace-infinite-item-selector",
        }, function (newElements) {
            var $godgrace_NewInfiniteScrollElems = $(newElements).css({
                opacity: 0
            });
            $(".fitVids").fitVids();
            godgrace_js_H_Center();
            godgrace_js_V_Center();
            godgrace_post_like();
            godgrace_share_post_links();
            godgrace_portfolio_items();
            godgrace_magnificPopup();
            godgrace_universal_filter_items_menu();
            godgrace_smooth_scroll();
            godgrace_sermons_single();

            $godgrace_NewInfiniteScrollElems.imagesLoaded(function () {
                $godgrace_NewInfiniteScrollElems.animate({
                    opacity: 1
                });

                if ($("#godgrace-item-entries").hasClass('isotope')) {
                    $godgrace_ContainerInfiniteScroll.isotope("appended", $godgrace_NewInfiniteScrollElems);
                }

            });
        });

    }

    /* Window load functions =================== */

    var $window = $(window);

    $(window).load(function () {

        var godgrace_sermon_video = $('.godgrace_sermon_video');
        if (godgrace_sermon_video.length) {
            var video_on = godgrace_sermon_video.html().replace('autoplay=0', 'autoplay=1');
            if ( document.location.href.indexOf('#getVideo') > -1 && video_on ) {           
                godgrace_sermon_video.html(video_on).show();           
            }
        }

        if ( $('.godgrace_site_loader').length ){
            $(".godgrace_site_loader").fadeOut("slow");
        }

        // Blog masonry grid
        function godgrace_BlogGridIsotope() {       
           if ($("#godgrace-item-entries").hasClass('isotope')) {
                $('.godgrace_blog_grid_sort').imagesLoaded( function() {
                    $('.godgrace_blog_grid_sort').isotope({
                        itemSelector: '.godgrace_blog_grid'
                    });
                });
            }            
        }

        // Global masonry
        function godgrace_UniversalGridIsotope() {       
           if ($("#godgrace-item-entries").hasClass('isotope')) {
                $('.godgrace_universal_grid_sort').imagesLoaded( function() {
                    $('.godgrace_universal_grid_sort').isotope({
                        itemSelector: '.godgrace_universal_grid'
                    });
                });
            }            
        }

        
        godgrace_BlogGridIsotope();
        godgrace_UniversalGridIsotope();       

        $window.resize(function () { 
            godgrace_BlogGridIsotope(); 
            godgrace_UniversalGridIsotope();  
        });
        window.addEventListener("orientationchange", function() { 
            godgrace_BlogGridIsotope(); 
            godgrace_UniversalGridIsotope();  
        });

        $('iframe').css('max-width','100%').css('width','100%');
        godgrace_infinite_scroll();

    });

})(jQuery);