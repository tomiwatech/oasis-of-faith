
$(document).ready(function () {

    "use strict";

    document.getElementById("submit_btn").addEventListener("click", function () {
        var name = document.getElementById("contact_name").value;
        var email = document.getElementById("contact_email").value;
        var message = document.getElementById("contact_message").value;
        var subject = "CONTACT OASIS OF FAITH";

        if(!name || !email || !message) {
            swal("Please Enter all required Information!", "", "error");
        }
        else {
            var formatedMessage = name  + "\n" + email + "\n" + message;
        
            $.ajax({
                url: "https://formspree.io/oasisoffaith2018@gmail.com",
                method: "POST",
                data: {
                    message: formatedMessage,
                    subject: subject
                },
                dataType: "json"
            });
       
            swal("Message successfully sent! Thanks for Reaching Us", "", "success");
            document.getElementById("contact_name").value = "";
            document.getElementById("contact_email").value = "";
            document.getElementById("contact_message").value = "";
        }

    });

  

})


